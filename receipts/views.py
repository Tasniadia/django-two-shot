from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# Create your views here.


@login_required
def receipt_home(request):
    receipt = Receipt.objects.all()
    context = {
        "receipt": receipt,
    }
    return render(request, "receipts/receipt.html", context)


@login_required
def my_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)

    context = {
        "my_receipts": receipts,
    }
    return render(request, "receipts/my_receipts.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)

    context = {"category": category}
    return render(request, "category/category.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category_item = form.save(False)
            category_item.owner = request.user
            category_item.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "category/create_category.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)

    context = {"accounts": accounts}
    return render(request, "account/account.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account_item = form.save(False)
            account_item.owner = request.user
            account_item.save()
            return redirect("account_list")
    else:
        form = AccountForm
    context = {
        "form": form,
    }
    return render(request, "account/create.html", context)
